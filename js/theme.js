/** Mogul Framework Javascript **/

// ---------------------------- Enable Console.log always
// usage: log('inside coolFunc', this, arguments);
// paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
window.log = function f(){ log.history = log.history || []; log.history.push(arguments); if(this.console) { var args = arguments, newarr; args.callee = args.callee.caller; newarr = [].slice.call(args); if (typeof console.log === 'object') log.apply.call(console.log, console, newarr); else console.log.apply(console, newarr);}};
(function(a){function b(){}for(var c="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,markTimeline,profile,profileEnd,time,timeEnd,trace,warn".split(","),d;!!(d=c.pop());){a[d]=a[d]||b;}})
(function(){try{console.log();return window.console;}catch(a){return (window.console={});}}());

//Detect iOS and add iPhone/iPad classes to html
var ua = window.navigator.userAgent;
var iOS = ua.match(/iPad/i) || ua.match(/iPhone/i);
var html = document.documentElement;
html.classList.add(iOS);

jQuery(document).ready(function($) {

    //Cart amount
    if($('.cart-amount').length != 0) {
        $.get('/wp-admin/admin-ajax.php?action=mogul_get_cart_count', function(data){
            $('.cart-amount').text(data);
        });
    }

    //lazy load all images
    if($('.lazy').length){
        $('.lazy').lazy();
    }

    //scroll to
    // HTML e.g <a href="#footer" data-offset="100">Go to footer</a>
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                //lets us set the vertical offest in px e.g data-offset="100"
                var attr = $(this).attr('data-offset');
                if( attr == undefined ){ attr = 0; }
                //console.log(attr);
                $('html,body').animate({ scrollTop: target.offset().top-attr }, 600);
                return false;
            }
        }
    });

    //Overide scrolljacking / prevent jankiness
    $('html,body').on("scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove", function() {
        $('html,body').stop();
    });


    //responsive Google Maps
    var googleMaps = 'iframe[src*="google.com"][src*="map"][src*="embed"]';
    $(googleMaps).wrap('<div class="google-map-wrapper disable-pointer-events"></div>').after('<style>.disable-pointer-events iframe{pointer-events:none;}</style>');
    $('.google-map-wrapper').on('mousedown', function(){ $(this).removeClass('disable-pointer-events'); });
    $('.google-map-wrapper').on('mouseleave', function(){ $(this).addClass('disable-pointer-events'); });

    // Touch enabled dropdown nav - doubletaptogo
    if ($(window).width() >=800 ){
        $( 'nav li:has(ul)' ).doubleTapToGo();
    }

    //Magnific

    $('.popup-video').magnificPopup({

        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false,
        iframe: {
            patterns: {
                wistia: {
                    index: 'wistia.com',
                    id: function(url) {
                        var m = url.split('/');
                        if (m !== null) {
                            return m[4];
                        }
                        return null;
                    },
                    src: '//fast.wistia.net/embed/iframe/%id%'
                },
                youtube: {
                    index: 'youtube.com/watch',
                    id: 'v=',
                    src: '//www.youtube.com/embed/%id%?rel=0&autoplay=1'
                }
            },
            srcAction: 'iframe_src',
        }
    });


    //Content Tabs
    $('ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
        window.location.hash = tab_id.substring(4);
    });

    //Youtube iframe in content
    $('iframe[src*="youtube.com"]').each(function() {
        $(this).wrap('<div class="video-wrapper"/>');
    });

    //Youtube videos in magnific
    $('a[href*="youtube/watch"]').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-fade',
        preloader: false,
        fixedContentPos: false
    });

    // Modal Popup
    $(document).on('click', 'a', function(event, ui) {
        var href = $(this).attr("href");
        //remove http[s]:// from the href
        var protomatch = /^https?:\/\/|#/;
        var popup_data_id = href.replace(protomatch, '');
        if($("[data-id='" + popup_data_id + "']").length != 0) {
            event.preventDefault();
            $('.popup').fadeOut();
            $('.popup[data-id="' + popup_data_id + '"]').fadeIn();
            $('html').addClass('popup-open');
            window.location.hash = '';
        }
    });

    // Hash Modal Popup
    if(window.location.hash) {
        var popup_data_id = encodeURIComponent(window.location.hash.substring(1));
        if($("[data-id='" + popup_data_id + "']").length != 0) {
            event.preventDefault();
            $('.popup').fadeOut();
            $('.popup[data-id="' + popup_data_id + '"]').fadeIn();
            $('html').addClass('popup-open');
        }
    }

    //close popup
    $('.popup .close').click(function(event){
        event.preventDefault();
        $(this).parents('.popup').fadeOut();
        $('html').removeClass('popup-open');
        window.location.hash = '';
    });



});//document.ready()

//Cookie helpers
function setCookie(name,value) {
    var expires = "";
    var date = new Date();
    date.setTime(date.getTime() + (365*24*60*60*1000));
    expires = "; expires=" + date.toUTCString();
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function eraseCookie(name) {
    document.cookie = name+'=; Max-Age=-99999999;';
}
