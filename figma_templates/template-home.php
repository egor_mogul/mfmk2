<?php /* Template Name: Home */

get_header();

while ( have_posts() ) :

	the_post();

?>

	<!-- Intro -->
		<?php
			$intro_layers = get_field('intro_layers');
			if(!empty($intro_layers)){
				foreach($intro_layers as $index => $layer){
					$theme_layer_path = get_stylesheet_directory() . '/layers/'.$layer['acf_fc_layout'];
					$framework_layer_path = get_template_directory() . '/layers/'.$layer['acf_fc_layout'];
					if(file_exists($theme_layer_path)) {
						include($theme_layer_path);
					} elseif(file_exists($framework_layer_path)) {
						include($framework_layer_path);
					} else {
						echo '<!-- No template found for ' . $layer['acf_fc_layout'] . ' -->';
					}
				}
			}
		?>

	<!-- Content -->
		<?php
			$general_layers = get_field('layers');
			if(!empty($general_layers)){
				foreach($general_layers as $index => $layer){
					$theme_layer_path = get_stylesheet_directory() . '/layers/'.$layer['acf_fc_layout'];
					$framework_layer_path = get_template_directory() . '/layers/'.$layer['acf_fc_layout'];
					if(file_exists($theme_layer_path)) {
						include($theme_layer_path);
					} elseif(file_exists($framework_layer_path)) {
						include($framework_layer_path);
					} else {
						echo '<!-- No template found for ' . $layer['acf_fc_layout'] . ' -->';
					}
				}
			}
		?>

	<!-- .content -->

<?php endwhile; get_footer(); ?>
