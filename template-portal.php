<?php

/*
 *
 * Template name: Portal
 *
 */

get_header();

while ( have_posts() ) :

	the_post();

?>

	<!-- Intro -->
		<?php
			$intro_layers = get_field('intro_layers');
			if(!empty($intro_layers)){
				foreach($intro_layers as $index => $layer){
					$theme_layer_path = get_stylesheet_directory() . '/layers/'.$layer['acf_fc_layout'];
					$framework_layer_path = get_template_directory() . '/layers/'.$layer['acf_fc_layout'];
					if(file_exists($theme_layer_path)) {
						include($theme_layer_path);
					} elseif(file_exists($framework_layer_path)) {
						include($framework_layer_path);
					} else {
						echo '<!-- No template found for ' . $layer['acf_fc_layout'] . ' -->';
					}
				}
			}
		?>


	<!-- Content -->
	<section class="layer portal">
		<div class="inner content">

			<?php if(!is_user_logged_in ()): ?>

				<section class="login" id="layer_0">
					<div class="inner clearfix">
						<?php echo do_shortcode( '[mogul_login]' ); ?>
					</div>
				</section><!-- .login -->

			<?php else: ?>

				<section class="tabs-wrap">
					<?php wp_nav_menu( array( 'theme_location' => 'portal-menu', 'container' => '' ) ); ?>
				</section>
				<?php $general_layers = get_field('layers'); ?>
				<?php if(!empty($general_layers)):	?>
					<?php foreach($general_layers as $index => $layer): ?>
						<?php
							$theme_layer_path = get_stylesheet_directory() . '/layers/'.$layer['acf_fc_layout'];
							$framework_layer_path = get_template_directory() . '/layers/'.$layer['acf_fc_layout'];
							if(file_exists($theme_layer_path)) {
								include($theme_layer_path);
							} elseif(file_exists($framework_layer_path)) {
								include($framework_layer_path);
							} else {
								echo '<!-- No template found for ' . $layer['acf_fc_layout'] . ' -->';
							}
						?>
					<?php endforeach; ?>
				<?php endif; ?>

			<?php endif; ?>

		</div>
	</section>
	<!-- .content -->

<?php endwhile; get_footer(); ?>
