<?php

/*
 * Portal
 */


register_nav_menu( 'portal-menu', 'Portal Menu' );


//Change the login logo
add_action( 'login_enqueue_scripts', 'mogul_framework_portal_login_logo' );
function mogul_framework_portal_login_logo() {
		?>
    		<style type="text/css">
        		.login h1 a {
            			background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/portal.png') !important;
	    			width:280px !important;
	    			height:101px !important;
	    			background-size: 110px 101px !important;
        		}
    		</style>
		<?php
}

//Redirect all login attempts to Portal page
//add_action('init','mogul_framework_portal_login_redirect');
function mogul_framework_portal_login_redirect(){
	global $pagenow;

  if( 'wp-login.php' == $pagenow  ) {
    if ( isset( $_POST['wp-submit'] ) ||   // in case of LOGIN
      ( isset($_GET['action']) && $_GET['action']=='logout') ||   // in case of LOGOUT
      ( isset($_GET['action']) && $_GET['action']=='rp') ||   // in case of PASSWORD RESET
      ( isset($_GET['action']) && $_GET['action']=='lostpassword') ||   // in case of LOGOUT
      ( isset($_GET['checkemail']) && $_GET['checkemail']=='confirm') ||   // in case of LOST PASSWORD
      ( isset($_GET['checkemail']) && $_GET['checkemail']=='registered') ) return;    // in case of REGISTER
    else {
         wp_redirect( home_url() );
         exit();
    }

	}
}

add_filter( 'wpmem_login_form_args',    'mogul_framework_portal_remove_wpmem_txt_code' );
add_filter( 'wpmem_register_form_args', 'mogul_framework_portal_remove_wpmem_txt_code' );
function mogul_framework_portal_remove_wpmem_txt_code( $args ){
	$args = array(
		'txt_before' => '',
		'txt_after'  => ''
	);
	return $args;
}

//On logout redirect to home page
add_action('wp_logout','mogul_framework_portal_go_home');
function mogul_framework_portal_go_home(){
	wp_redirect( home_url() );
	exit();
}

//Only allow admin access to dashboard
add_action( 'admin_init', 'mogul_framework_portal_init', 20 );
function mogul_framework_portal_init() {
  $user = wp_get_current_user();
  $file = basename($_SERVER['PHP_SELF']);
	if ( is_admin() && count(array_intersect($user->roles, array('administrator', 'client_administrator'))) == 0 && !(defined('DOING_AJAX') && DOING_AJAX ) && $file != 'admin-post.php' ) {
    wp_redirect( home_url() );
		exit;
	}

}

//Remove admin bar for non admin
add_action('init', 'mogul_framework_portal_remove_admin_bar');
function mogul_framework_portal_remove_admin_bar() {
    show_admin_bar(false);
    $user = wp_get_current_user();
	if (count(array_intersect($user->roles, array('administrator', 'client_administrator'))) || is_admin()) {
  	show_admin_bar(true);
	}
}



/*
 * Portal login form
 */

add_shortcode( 'mogul_login', 'mogul_framework_login_func' );
function mogul_framework_login_func() {
	$return = '';
	$redirect = ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  $return = '
		<div class="login-wrap">
			<div class="login-form">
        <form name="loginform" id="loginform" action="' . esc_url( site_url( 'wp-login.php', 'login_post' ) ) . '" method="post">
            <p class="login-username">
                <label for="user_login">Username or Email Address</label>
                <input type="text" name="log" id="user_login" class="input" value="" size="20" />
            </p>
            <p class="login-password">
                <label for="user_pass">Password</label>
                <input type="password" name="pwd" id="user_pass" class="input" value="" size="20" />
            </p>
            <p class="login-remember"><input name="rememberme" type="checkbox" id="rememberme" value="forever" /><label> Remember Me</label></p>
            <p class="login-submit">
                <input type="submit" name="wp-submit" id="wp-submit" class="button button-primary" value="Log In" />
                <input type="hidden" name="redirect_to" value="' . esc_url( $redirect ) . '" />
            </p>
        </form>
			</div><!-- .login-form -->
		</div><!-- .login-wrap -->';
	$return .= '<p>Forgot your password? Reset it <a href="/wp-login.php?action=lostpassword">here</a></p>';
	return $return;
}



/*
 * Portal User update info form
 */

add_shortcode( 'mogul_user_form', 'mogul_framework_portal_user_form_func' );
function mogul_framework_portal_user_form_func() {
	global $user;
	$return = '';
	$user = wp_get_current_user();
	$return = '<div class="form-wrap">';
	$return .= '<form method="post" id="update_user_form" action="'.esc_url( admin_url('admin-post.php') ) .'" enctype="multipart/form-data">';
	$return .= wp_nonce_field( 'check-update-user', 'update-user' );
	$return .= '<input name="action" type="hidden" id="action" value="mogul_update_user" />';
	$return .= '<input name="user_id" type="hidden" id="user_id" value="'.$user->ID.'" />';
	$return .= '<label>First Name <span>*</span></label>';
	$return .= '<input name="first_name" type="text" id="first_name" value="'.$user->first_name.'" class="textbox" required=""placeholder="First Name" />';
	$return .= '<label>Last Name <span>*</span></label>';
	$return .= '<input name="last_name" type="text" id="last_name" value="'.$user->last_name.'" class="textbox" required="" placeholder="Last Name" />';
	$return .= '<label>Email Address <span>*</span></label>';
	$return .= '<input name="user_email" type="email" id="user_email" value="'.$user->user_email.'" class="textbox" required="" placeholder="Email Address" />';
	$return .= '<label>Phone</label>';
	$return .= '<input name="phone1" type="text" id="phone1" value="'.$user->phone1.'" class="textbox" placeholder="Phone" />';
	$return .= '<label>Biographical Info</label>';
	$return .= '<textarea cols="20" rows="5" name="description" id="description" class="textarea">'.$user->description.'</textarea>';
	$return .= '<div class="button-group actions">';
	$return .= '<input name="updateuser" type="submit" id="updateuser" class="submit button" value="Save Profile" />';
	$return .= '</div><!-- .button-group -->';
	$return .= '</form>';
	$return .= '</div><!-- .form-wrap -->';
	return $return;
}

add_action( 'admin_post_nopriv_mogul_update_user', 'mogul_framework_portal_update_user' );
add_action( 'admin_post_mogul_update_user', 'mogul_framework_portal_update_user' );
function mogul_framework_portal_update_user() {
	global $current_user, $wpdb;
	$current_user = wp_get_current_user();

	if ( ! empty( $_POST ) ) {
		if ( isset( $_POST['update-user'] ) || wp_verify_nonce( $_POST['update-user'], 'check-update-user' ) ) {

			$user_update_args = array(
				'ID'         => $current_user->id,
			);

			if (isset( $_POST['email'])) {
    			// check if user is really updating the value
    			if ($current_user->user_email != $_POST['email']) {
    			     // check if email is free to use
    			    if (!email_exists( $_POST['email'] )){
    			        // Email exists, do not update value.
    							wp_redirect( home_url() );
									exit;
    			    } else {
    			        $user_update_args['user_email'] = esc_attr( $_POST['email'] );
    			        $user_update_args['billing_email'] = esc_attr( $_POST['email'] );
    			    }
    			}
    	}

			if (isset( $_POST['first_name']) && $current_user->first_name != $_POST['first_name']) {
				$user_update_args['first_name'] = esc_attr( $_POST['first_name'] );
    	}
			if (isset( $_POST['last_name']) && $current_user->last_name != $_POST['last_name']) {
				$user_update_args['last_name'] = esc_attr( $_POST['last_name'] );
    	}
			if (isset( $_POST['phone1']) && $current_user->phone1 != $_POST['phone1']) {
				update_user_meta( $current_user->ID, 'phone1', esc_attr($_POST['phone1']) );
    	}
			if (isset( $_POST['description']) && $current_user->description != $_POST['description']) {
				$user_update_args['description'] = esc_attr( $_POST['description'] );
    	}


			$user_id = wp_update_user( $user_update_args );

			if ( is_wp_error( $user_id ) ) {
    			wp_redirect( home_url() );
			} else {
    			wp_redirect( home_url() );
			}
			exit;
		}
	}
}


/*
 * Portal User update password form
 */

add_shortcode( 'mogul_pass_form', 'mogul_framework_portal_pass_form_func' );
function mogul_framework_portal_pass_form_func() {
	$return = '';
	$user = wp_get_current_user();
	$return = '<div class="form-wrap">';
	$return .= '<form method="post" id="reset_password" action="' . esc_url( admin_url('admin-post.php') ) . '" >';
	$return .= wp_nonce_field( 'check-reset-password', 'reset-password' );
	$return .= '<input name="action" type="hidden" id="action" value="mogul_change_password" />';
	$return .= '<label>New Password <span>*</span></label>';
	$return .= '<input name="new_user_pass" type="password" id="new_user_pass" class="textbox" required="" />';
	$return .= '<label>Confirm Password <span>*</span></label>';
	$return .= '<input name="confirm_user_pass" type="password" id="confirm_user_pass" class="textbox" required="" />';
	$return .= '<div class="button-group actions">';
	$return .= '<input name="resetpassword" type="submit" id="resetpassword" class="submit button" value="Change Password" />';
	$return .= '</div><!-- .button-group -->';
	$return .= '</form>';
	$return .= '</div><!-- .form-wrap -->';
	return $return;
}

add_action( 'admin_post_nopriv_mogul_change_password', 'mogul_framework_portal_change_password' );
add_action( 'admin_post_mogul_change_password', 'mogul_framework_portal_change_password' );
function mogul_framework_portal_change_password() {
	global $current_user, $wpdb;
	$current_user = wp_get_current_user();

	if ( ! empty( $_POST ) ) {
		if ( isset( $_POST['reset-password'] ) || wp_verify_nonce( $_POST['reset-password'], 'check-reset-password' ) ) {


			if($_POST['new_user_pass'] == '' || $_POST['confirm_user_pass'] == '') {
				// password(s) field empty
    		wp_redirect( home_url() );
			}

			if($_POST['new_user_pass'] != $_POST['confirm_user_pass']) {
				// passwords do not match
    		wp_redirect( home_url() );
			}

			$user_update_args = array(
				'ID'         => $current_user->id,
				'user_pass' => $_POST['new_user_pass']
			);

			$user_id = wp_update_user( $user_update_args );

			if ( is_wp_error( $user_id ) ) {
    			wp_redirect( home_url() );
			} else {
    			wp_redirect( home_url() );
			}
			exit;
		}
	}
}




/*
 * Portal User register form
 */

add_shortcode( 'mogul_register', 'mogul_framework_register_form_func' );
function mogul_framework_register_form_func() {
	$return = '';
	$user = wp_get_current_user();
	$return = '<div class="form-wrap">';
	if(isset($_GET['current_user'])){
		$return .= '<p class="current_user">There is already an account associated with this email address. Please check your inbox for details</p>';
	}
	$return .= '<form method="post" id="user_register_form" action="' . esc_url( admin_url( 'admin-post.php' ) ) . '" >';
	$return .= wp_nonce_field( 'check-user_register', 'user_register' );
	$return .= '<input name="action" type="hidden" id="action" value="mogul_user_register" />';
	$return .= '<label>Email Address <span>*</span></label>';
	$return .= '<input name="email" type="email" id="email" class="textbox" required="" />';
	$return .= '<label>First Name <span>*</span></label>';
	$return .= '<input name="first_name" type="text" id="first_name" class="textbox" required="" />';
	$return .= '<label>Last Name <span>*</span></label>';
	$return .= '<input name="last_name" type="text" id="last_name" class="textbox" required="" />';
	$return .= '<div class="button-group actions">';
	$return .= '<input name="register" type="submit" id="register" class="submit button" value="Register" />';
	$return .= '</div><!-- .button-group -->';
	$return .= '</form>';
	$return .= '</div><!-- .form-wrap -->';
	return $return;
}



add_action( 'admin_post_nopriv_mogul_user_register', 'mogul_framework_portal_user_register' );
add_action( 'admin_post_mogul_user_register', 'mogul_framework_portal_user_register' );
function mogul_framework_portal_user_register( $post_id ) {

	$return_url_attr = '';

	if ( ! empty( $_POST ) ) {
		if ( isset( $_POST['user_register'] ) || wp_verify_nonce( $_POST['user_register'], 'check-user_register' ) ) {

			//create a new user
			$email = $_POST['email'];
			$first_name = $_POST['first_name'];
			$last_name = $_POST['last_name'];

			$username = wp_slash( $email );
			$user_id = username_exists( $username );
			if ( !$user_id and email_exists($email) == false ) {
				$random_password = wp_generate_password( 8 );
				$user_id = wp_create_user( $username, $random_password, $email );
				if(!empty($user_id)){
  				wp_update_user(
    				array(
      				'ID'          =>    $user_id,
    					'first_name'  =>  $first_name,
    					'last_name'   =>  $last_name,
    				)
  				);
					wp_set_current_user ( $user_id );
					wp_set_auth_cookie  ( $user_id );
					wp_new_user_notification( $user_id, null, 'admin' );
					mogul_framework_portal_send_new_user_email( $user_id, $random_password );
				}
			} else {
				mogul_framework_portal_send_current_user_email($user_id);
				$return_url_attr = '?current_user';
			}

		}
		wp_redirect( $_POST["_wp_http_referer"] . $return_url_attr );
		exit;
	}
	wp_redirect( '/' );
	exit;
}



/*
 * New user email
 */

function mogul_framework_portal_send_new_user_email($user_id, $password){

	$user = get_userdata($user_id);
	$email = $user->user_email;

$text = 'Hi '.$user->first_name.' '.$user->last_name.',

Welcome to '.get_bloginfo( 'name' ).'
Your username: '.$user->user_login.'
Your password: '.$password.'
';

 wp_mail( $email, 'Welcome to '.get_bloginfo( 'name' ), $text);
}


/*
 * Current user email
 */

function mogul_framework_portal_send_current_user_email($user_id){

	$user = get_userdata($user_id);
	$email = $user->user_email;

$text = 'Hi '.$user->first_name.' '.$user->last_name.',

You already have have an account.
Use the forgotten password link <a href="'.get_site_url().'/wp-login.php?action=lostpassword">here</a>
';

 wp_mail( $email, 'Welcome to '.get_bloginfo( 'name' ), $text);
}
