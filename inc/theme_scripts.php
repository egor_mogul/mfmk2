<?php

// ----------------------------------------------------------------------------
// Add Theme Scripts
// ----------------------------------------------------------------------------

add_action( 'wp_enqueue_scripts', 'mogul_enqueue_scripts', 12 );
function mogul_enqueue_scripts() {

 		//Theme Styles
    wp_register_style( 'theme', get_stylesheet_directory_uri() . '/css/theme.css?v=' . md5_file(get_stylesheet_directory_uri() . '/css/theme.css'));
    wp_enqueue_style( 'theme' );

    wp_register_style( 'icons',  get_stylesheet_directory_uri() . '/css/icons.css');
    wp_enqueue_style( 'icons' );

    wp_register_style( 'fonts', 'https://fonts.googleapis.com/css?family=Work+Sans:400,700');
    wp_enqueue_style( 'fonts' );




 		//Theme Scripts
    wp_register_script( 'theme-js', get_stylesheet_directory_uri() . '/js/theme.js', array( 'jquery' ), '1.0', true);
    wp_enqueue_script( 'theme-js' );

   	//wp_register_script( 'typekit-js', 'https://use.typekit.net/vfj2eig.js', array(), null, true );
    //wp_enqueue_script( 'typekit-js' );

   	//wp_register_script( 'typekit-pre-js', get_stylesheet_directory_uri() .'/js/libs/typekit.js', array(), null, true );
    //wp_enqueue_script( 'typekit-pre-js' );


		//wp_register_script( 'gmaps-js', 'https://maps.googleapis.com/maps/api/js?key=API-KEY-HERE&libraries=places', array( 'jquery' ), '1.0', true);
		//wp_enqueue_script( 'gmaps-js' );

}


add_action('wp_enqueue_scripts', 'mogul_ajax_js');
function mogul_ajax_js() {
		wp_localize_script( 'jquery', 'mogul_vars',
			array(
				'mogul_ajax_url' 				=> admin_url( 'admin-ajax.php' ),
				'mogul_resource_url' 		=> get_stylesheet_directory_uri() . '/ajax/get_resource.php',
				'mogul_nonce' 					=> wp_create_nonce('mogul-nonce'),
				'mogul_resource_type' 	=> '', //this is used for the google search cpt
			)
		);
}

add_action( 'acf/init', 'my_acf_init' );
function my_acf_init() {
	acf_update_setting( 'google_api_key', 'API-KEY-HERE' );
}
