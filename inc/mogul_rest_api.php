<?php

/*
 * 
 *  Mogul Framework REST API functions
 *
 */



/*
 * Add ALL ACf Fields to a custom post type in REST API
 * Copy the commented code for each custom post type
 * Replace 'CPT' below with the slug of your custom post type
 */


//add_filter('rest_prepare_CPT', 'mogul_cpt_rest_acf_fields');

function mogul_cpt_rest_acf_fields($response) {
     $response->data['acf'] = get_fields($response->data['id']);
     return $response;
}


/*
 * Register API Menu
 */
register_nav_menu( 'API-menu', 'API Menu' );




/*
 * Make the API Pages post type require authetication in the REST API
 * Create a custom post type wiht the slug 'api_page'
 */


add_filter( 'rest_dispatch_request', 'mogul_block_api_pages', 10, 4 );
function mogul_block_api_pages( $dispatch_result, $request, $route, $hndlr ) {

	$target_base = '/wp/v2/api_page';    // Edit to your needs

	$pattern1 = untrailingslashit( $target_base ); // e.g. /wp/v2/api_page
	$pattern2 = trailingslashit( $target_base );   // e.g. /wp/v2/api_page/

	// Target only /wp/v2/api_page and /wp/v2/api_page/*
	if( $pattern1 !== $route && $pattern2 !== substr( $route, 0, strlen( $pattern2 ) ) )
		return $dispatch_result;

	// Additional permission check
	if( is_user_logged_in() )  // or e.g. current_user_can( 'manage_options' )
		return $dispatch_result;

	// Target GET method
	if( WP_REST_Server::READABLE !== $request->get_method() ) 
		return $dispatch_result;

	return new \WP_Error( 
		'rest_forbidden', 
		esc_html__( 'Sorry, you are not allowed to do that.', 'wpse' ), 
		[ 'status' => 403 ] 
	);

}