<?php







//TODO:
// 1. add a functions the checks if CSS/JS files exist anf write path to the template.










// ----------------------------------------------------------------------------
// Add Image Sizes
// ----------------------------------------------------------------------------

add_action( 'after_setup_theme', 'mogul_add_image_sizes' );
function mogul_add_image_sizes() {

    //add_image_size($name,$width,$height,$crop)
    add_image_size( 'thumbnail_no_crop', 200, 9999, false );

}


// ----------------------------------------------------------------------------
// Remove query strings for launch
// ----------------------------------------------------------------------------

//add_filter( 'script_loader_src', 'mogul_remove_script_version', 15, 1 );
//add_filter( 'style_loader_src', 'mogul_remove_script_version', 15, 1 );
function mogul_remove_script_version( $src ){
    $parts = explode( '?ver', $src );
    return $parts[0];
}
//=====================================================================================
// PHP class is in the repo(initial release)
//=====================================================================================


//=================================================================================================================================
//                                                    Abstract MFMK2 functions
//=================================================================================================================================


// PHP notes:
//
// // process overview for creating new layers:
// - copy folder from the library
// - rename the folder and its files
// - duplicate ACF, rename it and link to template
// - remove unused ACF in WP panel
// - ajust HTML layout if needed in layer_template.php
// - style in layers LESS-files
// - create the layer preview pic and copy the folder to the library if needed
// //
// FUNCTIONS:
// 1. field("field-name", optional "tag-name", optional "class") - shortcut for serviceField("fields")
//
// Used to output eithrer tags with custom class and values from ACF or just values if the last two parameters are empty.
//
//     + "field-name" ACF value - required
//     + "tag-name" tag in template - optional
//     + "class" CSS class -optional
//
// Calls universal serviceField function and pass parent fields group name.
// returns <tag-name class="class">field-name</tag-name>
//
// serviceField
// returns <tag-name class="class">field-name</tag-name>
//
//
// function option returns only value from options ACF group.
//

$repItem = null; //need this to get current repeating item visible instead of passing as an argument
// I'm already passing the link to a repeating item as an argument in all functions but don't have time for refactoring

//shortcuts
//shortcuts are calling serviceField with predefined arguments, this helps to keep the template code simple

function repeater($value, $container = NULL, $class = NULL){
   //call this within foreach loop in template to get a field value, see any repeating layer for reference(cards, tiles)
   global $repItem; //see $repItem = null comment
   echo serviceField($repItem, $value, $container, $class);
}

function field($value, $container = NULL, $class = NULL){
    //shortcut for accessing root filelds(title, subtitle, link etc)
    //NB: don't wrap WYSIWYG content into <p> tag
    global $layer;
    $parentGroup = $layer;
    echo serviceField($parentGroup, $value, $container, $class);
}

function option($value){
    //accessing layer options - see subtemplate - Options for DOMEntityReference
    //calling option("col-count") will check if there is a non-empty fields and returns its value if true
    //to add an option create a new ACF to the Options group
    //
    //the current options structure : no ACFs is the group, background setting are handled by its own function
    //
    //examples for layer options: items gutters, columns count, full-width etc.
    global $layer;
    $parentGroup = $layer["options"];
    echo serviceField($parentGroup, $value);
}

// base function that checks ASF and its value
function serviceField($fieldKey, $value, $container = NULL, $class = NULL){
    (isset($fieldKey[$value])) ? $value = $value = $fieldKey[$value] : $value = NULL;
    if($container && $value):
        return "<$container class=\"$class\">$value</$container>"; //value in a tag
    elseif (isset($value)):
        return $value; //just value
    endif;
}

function buttons($fieldKey = NULL, $class = NULL){
    global $layer;

    //similar to serviceField but with a different output structure. <footer> acts the same way as .button-group in the existing framework

    if (!$fieldKey){$fieldKey=$layer;} //look for button in root if the argument is NULL
    if(isset($fieldKey["buttons"]) && !empty($fieldKey["buttons"])) // checking if buttons repeater exists
    {
        $fieldKey = $fieldKey["buttons"];
        echo "<footer class=\"$class\">";
        foreach($fieldKey as $button):
            echo "<a href=\"" . $button["link"] . "\" class=\"button " . $button['size'] . " " . $button['color'] . " \">" . $button["text"] . "</a>";
        endforeach;
        echo "</footer>";
    }
    else
    {
        return NULL;
    }
}

$bgClass = function() {
    //
    //Handles layer background class, if need to add just a class, create select/text/radio/whatever ACF in Options and access this using option('fieldName');
    //
    //add ACF ["bg_type"] if needed e.g. video.
    //then style in layer LESS
    //removing a "-bg-*" field grom ACF won't break anything

    // ACF structure example:
    // [bg_settings] => Array
    //     (
    //         [bg_type] => -bg-blank
    //         [image] => http://mfmk2.wpengine.com/wp-content/uploads/2019/01/fallback-share-thumbnail-mogul.png
    //         [bg_color] => -bg-blue
    //         [bg_gradient] => Array
    //             (
    //             )
    //
    //         [color] => -text-light
    //     )

    global $layer;
    $output = NULL;
    $bg_type = $layer["options"]["bg_settings"];
    switch ($bg_type["bg_type"]){
        //checking what kind of background we've got and output appropriate field value
        //tricky moment - "-bg-*" CSS class affects layer's spacing.
        //I.e. two layers with a background usually don't have space between and this is handled by CSS.
        case "-bg-blank": $output = "-blank"; break;
        case "-bg-image": $output = $bg_type["bg_type"] . " " . $bg_type["color"]; break;
        case "-bg-color": $output = $bg_type["bg_color"] . " " . $bg_type["color"]; break;
        case "-bg-gradient": $output = $bg_type["bg_gradient"] . ' ' . $bg_type["color"]; break;
    };
    echo $output;
};

//returns <img>  stored in options with default Large size
$bgImage = function(){
    //bg image for a section, same checks as above but different output
    global $layer;
    if( $layer["options"]["bg_settings"]["image"]){
    $url = $layer["options"]["bg_settings"]["image"];
    if($layer["options"]["bg_settings"]["bg_type"] == "-bg-image"):  echo " style=\"background-image:url($url)\" "; endif;
}
};

function isLink($state, $parent = NULL){
    //clunky but works
    //pass repeating item within loops
    //BUG: nested <a>s will cause DOM glitch. So be sure you dont have any <a>s between opening and closing isLink() calls.
    global $fields;
    global $repItem;

    if (!$parent){$parent=$fields;}

    (isset($parent["link"])) ? (!empty($parent["link"]) ? $link = $parent["link"] : $link = null) : $link = null;

    if($link){
        if($state == "open") {echo "<a href=\"$link\">";}
        if($state == "close") {echo "</a>";}
    }
}

//returns <img> with default Large size

function mediaSource($fieldKey = NULL){
    //returns path to the image

    //to add size argument and output.
    global $layer;
    if(!$fieldKey){ $fieldKey = $layer;}

    if(isset($fieldKey["image"]) && !empty($fieldKey["image"]))
    {
        $img_id = $fieldKey["image"];
        //$size =  $fieldKey["image_size"]; //just commented code
        return wp_get_attachment_image_src($img_id, "large")[0];
    }
    else
    {
        return NULL;
    }
}
