<?php

/*
 * Woocommerce
 */


//Only do things below here if Woocommerce is active
if ( class_exists( 'WooCommerce' ) ) {

	// Remove each style one by one
	add_filter( 'woocommerce_enqueue_styles', 'mogul_dequeue_styles' );
	function mogul_dequeue_styles( $enqueue_styles ) {
		unset( $enqueue_styles['woocommerce-general'] );	// Remove the gloss
		unset( $enqueue_styles['woocommerce-layout'] );		// Remove the layout
		unset( $enqueue_styles['woocommerce-smallscreen'] );	// Remove the smallscreen optimisation
		return $enqueue_styles;
	}
	// Or just remove them all in one line
	add_filter( 'woocommerce_enqueue_styles', '__return_false' );


	/** Disable Ajax Call from WooCommerce on front page and posts*/
	add_action( 'wp_enqueue_scripts', 'dequeue_woocommerce_cart_fragments', 11);
	function dequeue_woocommerce_cart_fragments() {
		if (is_front_page() || is_single() ) wp_dequeue_script('wc-cart-fragments');
	}


	/** Disable All WooCommerce  Styles and Scripts Except Shop Pages*/
	add_action( 'wp_enqueue_scripts', 'dequeue_woocommerce_styles_scripts', 99 );
	function dequeue_woocommerce_styles_scripts() {
		if ( function_exists( 'is_woocommerce' ) ) {
			if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
				# Styles
				wp_dequeue_style( 'woocommerce-general' );
				wp_dequeue_style( 'woocommerce-layout' );
				wp_dequeue_style( 'woocommerce-smallscreen' );
				wp_dequeue_style( 'woocommerce_frontend_styles' );
				wp_dequeue_style( 'woocommerce_fancybox_styles' );
				wp_dequeue_style( 'woocommerce_chosen_styles' );
				wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
				# Scripts
				wp_dequeue_script( 'wc_price_slider' );
				wp_dequeue_script( 'wc-single-product' );
				wp_dequeue_script( 'wc-add-to-cart' );
				wp_dequeue_script( 'wc-cart-fragments' );
				wp_dequeue_script( 'wc-checkout' );
				wp_dequeue_script( 'wc-add-to-cart-variation' );
				wp_dequeue_script( 'wc-single-product' );
				wp_dequeue_script( 'wc-cart' );
				wp_dequeue_script( 'wc-chosen' );
				wp_dequeue_script( 'woocommerce' );
				wp_dequeue_script( 'prettyPhoto' );
				wp_dequeue_script( 'prettyPhoto-init' );
				wp_dequeue_script( 'jquery-blockui' );
				wp_dequeue_script( 'jquery-placeholder' );
				wp_dequeue_script( 'fancybox' );
				wp_dequeue_script( 'jqueryui' );
			}
		}
	}

	add_action('wp_ajax_mogul_get_cart_count', 'mogul_get_cart_count_callback');
	add_action('wp_ajax_nopriv_mogul_get_cart_count', 'mogul_get_cart_count_callback');
	function mogul_get_cart_count_callback() {
		echo WC()->cart->get_cart_contents_count(); 
		exit;
	}


/*
	add_filter('wc_add_to_cart_message_html', 'mogul_order_message', 10, 2);
	function mogul_order_message($message, $product_id) {
    $name_output = null;
     $variation_id = isset( $_REQUEST[ 'variation_id' ] ) ? $_REQUEST[ 'variation_id' ] : null;
    // Collect the product, product variations and attributes
		if(!empty($variation_id)){
    $var_product = wc_get_product( $variation_id );
    $variations = $var_product->get_variation_attributes();
    $attributes = $var_product->get_attributes();

    if ( is_array( $variations ) ) {
			foreach( $variations as $key => $value ) {
				$key = str_replace( 'attribute_', '', $key ); // Clean the attribute name
				$attribute = $attributes[$key]; // Get the attribute data
				// Check if the attribute is a taxonomy
				if( isset( $attribute['is_taxonomy'] ) ){
					// Get the taxonomy name
					$attr_name = get_term_by( 'slug', $value, $key, 'ARRAY_A' );
					$attr_name = $attr_name['name'];
				} else {
					$attr_name = ucwords($value); // Clean up the custom attribute name
				}
				$name_output[] = $attr_name; // Load them into an array to be imploded
			}
		}
		}

    $product_title = get_the_title( $product_id ); // Get the main product title
    $product_title .= ( $name_output ? ' - ' . implode( ', ', $name_output ) : '' ); // Add variation(s) if not null
    $added_text = sprintf( __( '"%s" was successfully added to your basket.', 'woocommerce' ), $product_title );

    // Output success messages
    $message    = sprintf( '<a href="'.get_permalink(get_option( 'woocommerce_cart_page_id' )).'" class="button wc-forward">View Basket</a> %s', $added_text );
    return $message;
	}
*/

	add_filter( 'woocommerce_min_password_strength', 'mogul_woocommerce_min_password_strength', 10, 1 );
	function mogul_woocommerce_min_password_strength( $int ) { 
    $int = 2;
    return $int; 
	}; 


}