<?php ini_set('display_errors',1); ini_set('display_startup_errors',1); error_reporting(-1); ?>
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title><?php wp_title(''); ?></title>

    <?php //Favicons – setup via realfavicongenerator.net ?>
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon/favicon-16x16.png">
	<meta name="msapplication-TileColor" content="#00c5e1">
	<meta name="theme-color" content="#00c5e1">

	<meta name="viewport" content="width=device-width">

    <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
        <?php do_action('after_body_tag'); //required for mogul-seo plugin ?>
		<?php
			$header_layers = get_field('header_layers', 'option');
			if(!empty($header_layers)){
				foreach($header_layers as $index => $layer){
					$theme_layer_path = get_stylesheet_directory() . '/layers/'.$layer['acf_fc_layout'];
					$framework_layer_path = get_template_directory() . '/layers/'.$layer['acf_fc_layout'];
					if(file_exists($theme_layer_path)) {
						include($theme_layer_path);
					} elseif(file_exists($framework_layer_path)) {
						include($framework_layer_path);
					} else {
						echo '<!-- No template found for ' . $layer['acf_fc_layout'] . ' -->';
					}
				}
			}
		?>
