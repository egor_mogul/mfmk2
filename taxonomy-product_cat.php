<?php

/**
 * The Template for displaying products in a product category.
 *
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
/*
	Mogul modifications - full custom template to replace the default wc_get_template( 'archive-product.php' );
*/

get_header();

echo 'jeromeo';

$tax = 'product_cat';

$term_id = get_queried_object()->term_id;
$term = get_term($term_id, $tax);

$thumbnail_id = get_term_meta( $term_id, 'thumbnail_id', true );
$image = wp_get_attachment_url( $thumbnail_id );
$background_image = $image ? 'background-image:url(' . $image . ')' : '';

$parent = mogul_framework_get_term_top_most_parent($term_id, $tax);
$parent_thumbnail_id = get_term_meta( $parent->term_id, 'thumbnail_id', true );
$parent_image = wp_get_attachment_url( $parent_thumbnail_id );

if( $background_image == '' && !empty($parent_image) ){
	$background_image = 'background-image:url(' . $parent_image . ')';
}

?>

	<!-- Intro -->
		<section class="intro intro-default background-image" style="<?php echo $background_image; ?>">
			<div class="inner pad-top-100 pad-bot-40">
				<div class="intro-copy dark inner-700">
						<ul class="breadcrumbs">
							<?php echo mogul_framework_custom_breadcrumbs(); ?>
						</ul>
						<h1><?php echo $term->name; ?></h1>
      			<?php echo apply_filters('the_content', $term->description); ?>
				</div><!-- .intro-copy -->
			</div><!-- .inner -->
		</section> <!-- .intro -->

	<!-- Content -->
<?php

	$args = array(
		'posts_per_page'  => -1,
		'orderby'				 	=> 'menu_order',
		'order'				 		=> 'ASC',
		'post_type'				=> 'product',
		'tax_query' => array(
			array(
				'taxonomy' => $tax,
				'field' => 'slug',
				'terms' => $term->slug
			)
		)
	);

	$products = get_posts( $args );

?>

	<section class="layer taxonomy-tiles products">
		<div class="inner pad-top-60 pad-bot-60 ">
			<div class="grid flex column-4">
				<?php foreach( $products as $post ): ?>

  				<div class="col tile shadow product">
						<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );?>
						<a href="<?php the_permalink(); ?>">
    					<img src="<?php echo $image[0]; ?>" data-id="<?php echo $post->ID; ?>">
						</a>
    				<div class="tile-copy">
      				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							<p><?php echo mogul_framework_post_excerpt($post->ID, 100); ?></p>
							<a href="<?php the_permalink(); ?>" class="button small">Read more</a>
    				</div><!-- .tile-copy -->
  				</div><!-- .col -->

				<?php endforeach; ?>
				<?php wp_reset_query(); ?>
			</div><!-- .grid -->
		</div><!-- .inner -->
	</section><!-- .layer -->
	<!-- .content -->

<?php get_footer(); ?>
