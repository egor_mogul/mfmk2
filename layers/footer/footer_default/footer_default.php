<?php if( mogul_framework_layer_is_visible($layer) ): ?>
    <?php
        $layer_id = mogul_framework_get_layer_id_by_template_path($layer['acf_fc_layout']);
        $meta_id = mogul_framework_get_layer_meta_id($layer_id);
        $layer_name = '';
        if(!empty($meta_id)) $layer_name = str_replace('Layer – ','', get_the_title($meta_id));
    ?>
	<footer class="layer dark footer" data-layer-id="<?php echo $layer_id; ?>" data-layer-path="<?php echo $layer['acf_fc_layout']; ?>">
		<div class="inner">
            <?php if(!empty($layer["columns"])): ?>
			<div class="grid column-<?php echo count( $layer["columns"] ); ?>">
    				<?php foreach( $layer["columns"] as $column ): ?>
    					<div class="col">
    						<h4><?php echo $column["title"]; ?></h4>
    						<?php echo $column["content"]; ?>
    						<?php if( $column["type"] == "links" && !empty($column["links"]) ): ?>
    							<ul>
    								<?php foreach( $column["links"] as $link ): ?>
    									<li><a href="<?php echo $link["link"]; ?>" <?php if(!empty( $link["new_window"])) echo 'target="_blank"'; ?>><?php echo $link["text"]; ?></a></li>
    								<?php endforeach; ?>
    							</ul>
    						<?php endif; ?>
    						<?php if( $column["type"] == "newsletter" ): ?>
    							<form>
    								<input type="text" name="" placeholder="Name">
    								<input type="email" name="" placeholder="Email">
    								<input type="submit" name="" value="Submit">
    							</form>
    						<?php endif; ?>
    						<?php if( $column["type"] == "social" && !empty($column["social"]) ): ?>
    							<div class="button-group social">
    								<?php foreach( $column["social"] as $social ): ?>
    									<a href="<?php echo $social["link"]; ?>" target="_blank"><i class="mdi mdi-<?php echo $social["icon"]; ?>"></i></a>
    								<?php endforeach; ?>
    							</div>
    						<?php endif; ?>
    						<?php if( $column["type"] == "logo" && !empty($column["logo"]) ): ?>
    							<img src="<?php echo $column["logo"]["sizes"]["medium"]; ?>" alt="<?php echo $column["logo"]["alt"]; ?>">
    						<?php endif; ?>
    					</div><!-- .col -->
    				<?php endforeach; ?>

                </div><!-- .grid -->

        <?php endif; ?>

		<div class="footer-meta clearfix">
			<p class="footer_meta_text"><?php echo strip_tags(apply_filters('the_content', $layer["footer_meta"])); ?> <a href="https://mogul.nz/" target="_blank">Website by Mogul</a></p>
		</div><!-- .footer-meta -->



	</div><!-- .inner -->
</footer><!-- .layer -->
</div><!--.wrap -->
<?php else: ?>
    <!-- <?php echo $layer['acf_fc_layout']; ?> hidden by visibility settings or lack of content -->
<?php endif; ?>
