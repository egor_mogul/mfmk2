<?php if(mogul_framework_layer_is_visible($layer)): ?>
<?php
    $layer_id = mogul_framework_get_layer_id_by_template_path($layer['acf_fc_layout']);
    $meta_id = mogul_framework_get_layer_meta_id($layer_id);
    $layer_name = '';
    if(!empty($meta_id)) $layer_name = str_replace('Layer – ','', get_the_title($meta_id));
?>

<!--

    <form role="search" method="get" class="search-form" action="<?php bloginfo('url'); ?>">
		<label>
			<span class="screen-reader-text">Search for:</span>
			<input type="search" class="search-field" placeholder="Type to search..." value="" name="s" title="Search for:" onblur="if (this.value == '') {this.value = 'Type to search...';}" onfocus="if (this.value == 'Type to search...') {this.value = '';}" autocomplete="off">
			<small>Press 'Enter' to search or <a href="#close" class="close">close</a></small>
		</label>
		<input type="submit" class="search-submit" value="Search">
	</form>
	<nav class="mobile-nav">
		<i class="mdi mdi-close"></i>
		<?php wp_nav_menu( array( 'theme_location' => 'mobile-menu', 'container' => '' ) ); ?>
	</nav>
	<div class="wrap">
		<header id="header" class="flex flex-nowrap flex-align-center layer" data-layer-id="<?php echo $layer_id; ?>" data-layer-path="<?php echo $layer['acf_fc_layout']; ?>">
			<a class="main-logo" href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a>
			<div class="flex flex-grow"></div>
			<div class="header-inner flex flex-nowrap flex-align-center">
				<nav class="flex flex-nowrap flex-align-center">
					<?php wp_nav_menu( array( 'theme_location' => 'main-menu', 'container' => '' ) ); ?>
				</nav>
				<i class="mdi mdi-magnify"></i>
				<?php if ( class_exists( 'WooCommerce' ) ) : ?>
  				<a href="/shop/cart"><i class="mdi mdi-cart"></i><span class="cart-amount"></span></a>
				<?php endif; ?>
				<i class="mdi mdi-menu"></i>
			</div>
		</header>
--->


        <div class="header-container">
              	<a class="logo" href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a>
    			<nav class="desktop">
					<?php wp_nav_menu( array( 'theme_location' => 'main-menu', 'container' => '' ) ); ?>
    			</nav>

			<?php if ( class_exists( 'WooCommerce' ) ) : ?>
  				<a href="/shop/cart"><i class="mdi mdi-cart"></i><span class="cart-amount"></span></a>
				<?php endif; ?>
				<i class="mdi mdi-menu"></i>

    			<a href="#" class="search"><i class="icon icon-search"></i></a>

    			<button class="hamburger hamburger--slider" type="button" id="show-menu">
    			  <span class="hamburger-box">
    			    <span class="hamburger-inner"></span>
    			  </span>
    			</button>

    			<div class="search-form" role="search" method="get" >
    				<form action="<?php bloginfo('url'); ?>">>
    					<input type="text" name="" value="">
    					<button>Search</button>
    				</form>
    			</div>

                <nav class="mobile">
            		<i class="close-button" id="hide-menu"></i>
            		<?php wp_nav_menu( array( 'theme_location' => 'mobile-menu', 'container' => '' ) ); ?>
            	</nav><!-- .mobile-nav -->

    		</header>
    	</div>


<?php else: ?>
    <!-- <?php echo $layer['acf_fc_layout']; ?> hidden by visibility settings -->
<?php endif; ?>
