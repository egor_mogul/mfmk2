
<?php if(mogul_framework_layer_is_visible($layer)):

// CURRENT MOGUL FRAMEWORK CODE
$layer_id = mogul_framework_get_layer_id_by_template_path($layer['acf_fc_layout']);
$meta_id = mogul_framework_get_layer_meta_id($layer_id);
$layer_name = '';


if(!empty($meta_id)) $layer_name = str_replace('Layer – ','', get_the_title($meta_id));
    ?>
<!-- html -->
<?php else: ?>
    <!-- <?php echo $layer['acf_fc_layout']; ?> hidden by visibility settings -->
<?php endif; ?>

<?php $css_path = str_replace('.php', '.css', $layer['acf_fc_layout']); //layer CSS path ?>
<link rel="stylesheet" href="/wp-content/themes/theme/layers/<?php echo $css_path; ?>">

<section class="intro -intro-default <?php $bgClass(); ?>" <?php $bgImage(); ?>  id="layer_<?php echo $index; ?>" data-layer-id="<?php echo $layer_id; ?>" data-layer-name="<?php echo $layer_name; ?>"
data-layer-meta="<?php echo $meta_id; ?>" data-layer-path="<?php echo $layer['acf_fc_layout']; ?>">

<div class="grid__ -zero-gutter _col-<?php echo option("column_count"); ?>">
</div>

    <div class="inner">
                <div>
                    <div class="hgroup">
                        <?php field("title", "h2"); ?>
                        <?php field("subtitle", "p"); ?>
                    </div>
                    <?php buttons();?>
                </div>

    </div><!-- .inner -->
</section><!-- .layer -->
