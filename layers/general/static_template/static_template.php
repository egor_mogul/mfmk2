
<?php if(mogul_framework_layer_is_visible($layer)): ?>

<?php
// CURRENT MOGUL FRAMEWORK CODE
$layer_id = mogul_framework_get_layer_id_by_template_path($layer['acf_fc_layout']);
$meta_id = mogul_framework_get_layer_meta_id($layer_id);
$layer_name = '';

if(!empty($meta_id)) $layer_name = str_replace('Layer – ','', get_the_title($meta_id));?>
<?php else: ?>
    <!-- <?php echo $layer['acf_fc_layout']; ?> hidden by visibility settings -->
<?php endif;?>

<?php

//$options = $layer["options"];?>

<?php $css_path = str_replace('.php', '.css', $layer['acf_fc_layout']); //layer CSS path
if(file_exists($css_path)){
    echo "<link rel=\"stylesheet\" href=\"/wp-content/themes/theme/layers/<?php echo $css_path; ?>\">";
}
?>



<?php
// .layer - layer block
// .intro_ layer Type
// ._dark - functional modificator
// .-optional layer-specific modificator(should be configured in ACF option fields) i.e. .-bg-image, .-blue-gradient etc.
?>

<pre>
    <?php
    // array structure
    // $output = new stuff($layer);
    //$repeaters = $layer["items"];
    ?>
</pre>


<section class="layer <?php $bgClass(); ?>" <?php $bgImage(); ?> id="layer_<?php echo $index; ?>" data-layer-id="<?php echo $layer_id; ?>" data-layer-name="<?php echo $layer_name; ?>"
data-layer-meta="<?php echo $meta_id; ?>" data-layer-path="<?php echo $layer['acf_fc_layout']; ?>">
    <div class="inner">
        <div class="grid__">
            <div class="col">
                <div>
                    <div class="hgroup">
                        <?php field("title", "h1", "small"); ?>
                        <?php field("subtitle", "p"); ?>
                    </div>
                    <?php field("content"); ?>
                    <?php buttons();?>
                </div>
            </div>
            <?php if(mediaSource()): ?>
                <div class="col media">
                    <div>
                        <?php isLink("open"); ?>
                        <figure class="media" style="background:url(<?php echo mediaSource(); ?>);">
                            <img src="<?php echo mediaSource(); ?>" alt="">
                        </figure>
                        <?php isLink("close"); ?>
                    </div>
                </div>
            <?php endif; ?>
          </div>
    </div><!-- .inner -->
</section><!-- .layer -->
