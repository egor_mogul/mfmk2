
<?php if(mogul_framework_layer_is_visible($layer)):

// CURRENT MOGUL FRAMEWORK CODE
$layer_id = mogul_framework_get_layer_id_by_template_path($layer['acf_fc_layout']);
$meta_id = mogul_framework_get_layer_meta_id($layer_id);

if(!empty($meta_id)) $layer_name = str_replace('Layer – ','', get_the_title($meta_id));
    ?>
<!-- html -->
<?php else: ?>
    <!-- <?php echo $layer['acf_fc_layout']; ?> hidden by visibility settings -->
<?php endif; ?>

<?php
   // $output = new stuff($layer);
    $repeaters = $layer["items"];
?>

<?php $css_path = str_replace('.php', '.css', $layer['acf_fc_layout']); //layer CSS path ?>
<link rel="stylesheet" href="/wp-content/themes/theme/layers/<?php echo $css_path; ?>">

<pre>
TEst:


    <?php print_r($repeaters); ?>
</pre>

<section class="layer  tiles_ -shadow <?php $bgClass(); ?>" <?php $bgImage(); ?> id="layer_<?php echo $index; ?>" data-layer-id="<?php echo $layer_id; ?>" data-layer-name="<?php echo $layer_name; ?>"
data-layer-meta="<?php echo $meta_id; ?>" data-layer-path="<?php echo $layer['acf_fc_layout']; ?>">
    <div class="inner">

                <div>
                    <div class="hgroup">
                        <?php field("title", "h1", "big"); ?>
                        <?php field("subtitle", "p"); ?>
                    </div>
                    <?php //field("content"); ?>
                    <?php buttons();?>
                </div>

                  <div class="grid__  _col-<?php echo option("column_count"); ?>">
                     <?php

                     // Chris to write a function for repeaters.
                     // here goes alternating cloned sub-layer

                     foreach ($repeaters as $index => $repeater): ?>

                        <?php
                            global $repItem;
                            $repItem = $repeater;
                            $repImage = mediaSource($repeater);
                        ?>

                         <div class="col">
         						<!-- repeater/ -->
         						<div>
                                    <?php if($repImage): ?>
                                        <div class="media">
                                                <?php isLink("open"); ?>
                                                <figure class="media" style="background:url(<?php echo $repImage; ?>);">
                                                    <?php // manage images with CSS or just delete either bg or img ?>
                                                    <img src="<?php echo $repImage; ?>" alt="">
                                                </figure>
                                                <?php isLink("close"); ?>
                                        </div>
                                    <?php endif; ?>

         							<div class="content">
         								<div class="hgroup">
                                            <?php
                                            //see readme/theme_funcs
                                            repeater("title", "h1", "big"); ?>
                                            <?php repeater("subtitle", "p"); ?>
         								</div>
                                        <?php repeater("content"); ?>
                                        <?php buttons($repItem);?>
         							</div>
         						</div>
                         </div>
                     <?php endforeach;?>

                    </div>
    </div><!-- .inner -->
</section><!-- .layer -->
