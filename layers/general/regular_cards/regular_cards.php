
<?php if(mogul_framework_layer_is_visible($layer)):

// CURRENT MOGUL FRAMEWORK CODE
$layer_id = mogul_framework_get_layer_id_by_template_path($layer['acf_fc_layout']);
$meta_id = mogul_framework_get_layer_meta_id($layer_id);
$layer_name = '';


if(!empty($meta_id)) $layer_name = str_replace('Layer – ','', get_the_title($meta_id));
    ?>
<!-- html -->
<?php else: ?>
    <!-- <?php echo $layer['acf_fc_layout']; ?> hidden by visibility settings -->
<?php endif; ?>

<?php
   // $output = new stuff($layer);
    $repeaters = $layer["items"];
?>

<?php $css_path = str_replace('.php', '.css', $layer['acf_fc_layout']); //layer CSS path ?>
<link rel="stylesheet" href="/wp-content/themes/theme/layers/<?php echo $css_path; ?>">

<section class="layer cards <?php $bgClass(); ?>" <?php $bgImage(); ?> id="layer_<?php echo $index; ?>" data-layer-id="<?php echo $layer_id; ?>" data-layer-name="<?php echo $layer_name; ?>"
data-layer-meta="<?php echo $meta_id; ?>" data-layer-path="<?php echo $layer['acf_fc_layout']; ?>">
    <div class="inner">

                <div>
                    <div class="hgroup">
                        <?php field("title", "h2"); ?>
                        <?php field("subtitle", "p"); ?>
                    </div>
                    <?php //field("content"); ?>
                    <?php buttons();?>
                </div>

                <?php //_col-4 - hardcoded max col number ?>
                  <div class="grid__  _col-3">
                     <?php

                     // Chris to write a function for repeaters.
                     // here goes alternating cloned sub-layer
                     if ($repeaters):
                     foreach ($repeaters as $index => $repeater): ?>
                        <?php
                            global $repItem;
                            $repItem = $repeater;
                            $repImage = mediaSource($repItem);
                        ?>

                         <div class="col card">
         						<!-- repeater/ -->
         						<div>
                                    <?php if($repImage): ?>
                                        <?php isLink("open", $repItem); ?>
                                        <div class="media"  style="background-image:url(<?php echo $repImage; ?>);">
                                            <?php //inline svg is for keeping aspect ratio for cards, can be deleted if div.media size specified in CSS for each breakpoint ?>
                                            <svg viewBox="0 0 320 180" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="320" height="180" fill="#fff0"></rect></svg>
                                            <div class="card-copy">
                                                <?php repeater("title", "h4"); ?>
                                                <?php repeater("content"); ?>
                                                <?php //buttons($repItem); // don't include links into other links ?>
                                            </div>
                                        </div>
                                        <?php isLink("close", $repItem); ?>
                                    <?php endif; ?>
         						</div>
                         </div>
                     <?php endforeach;
                     endif;
                     ?>

                    </div>
    </div><!-- .inner -->
</section><!-- .layer -->
