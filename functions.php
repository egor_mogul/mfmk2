<?php

// ----------------------------------------------------------------------------
// Theme
// ----------------------------------------------------------------------------

define('MOGUL_PORTAL', TRUE);


/*
 * Setup themes functions folder
 */
define('MOGUL_THEME_FUNC',  get_stylesheet_directory(). '/inc' );


/*
 * Add theme scripts
 */
require_once ( MOGUL_THEME_FUNC . '/theme_scripts.php' );


/*
 * Theme custom functions
 */
require_once ( MOGUL_THEME_FUNC . '/theme_funcs.php' );


/*
 * Woocommerce changes
 */
require_once ( MOGUL_THEME_FUNC . '/theme_woocommerce.php' );


/*
 * Portal
 * Use the template
 */
if(MOGUL_PORTAL == TRUE){
	require_once ( MOGUL_THEME_FUNC . '/theme_portal.php' );
}


/*
 * Rest API
 */
require_once ( MOGUL_THEME_FUNC . '/mogul_rest_api.php' );


/*
 * Remove unwanted templates
 */
add_filter( 'theme_page_templates', 'tfc_remove_page_templates' );
function tfc_remove_page_templates( $templates ) {
	//Remove Portal template if unwanted
	if(MOGUL_PORTAL == FALSE){
    unset( $templates['template-portal.php'] );
	}
	//Remove Woocommerce template if unactive
	if( !class_exists( 'woocommerce' ) ) {
    unset( $templates['template-woocommerce.php'] );
	}

	return $templates;
}


add_filter( 'wp_fatal_error_handler_enabled', '__return_false' );//do not email the site admin on 500 errors
