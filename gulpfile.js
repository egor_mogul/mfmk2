var gulp = require('gulp');
var less = require('gulp-less');
var sourcemaps = require('gulp-sourcemaps');
var log = require('fancy-log');
var color = require('gulp-color');
var cleanCSS = require('clean-css');

gulp.task('watch', (e) => {
  //gulp.watch('src/js/*.js', ['scripts']);
  gulp.series('compile-theme', 'compile-layers')();
  log(color('Initial compication is done', 'RED'));

  log(color('Night gathers, and now my watch begins. It shall not end until my death. I shall take no wife, hold no lands, father no children. I shall wear no crowns and win no glory. I shall live and die at my post. I am the sword in the darkness. I am the watcher on the walls. I am the shield that guards the realms of men. I pledge my life and honor to the Night\'s Watch, for this night and all the nights to come.', 'YELLOW'));
  gulp.watch('./css/theme.less', gulp.series('compile-theme'));
  gulp.watch('./css/inc/*.less', gulp.series('compile-theme'));
  gulp.watch('./layers/**/*.less', gulp.series('compile-layers'));
});


gulp.task('compile', (e) => {
    gulp.series('compile-theme', 'compile-layers')();
    e();
})


gulp.task('compile-theme', () => {
    log('Starting theme compilation' + '\n')
    return gulp.src('./css/theme.less')
        .pipe(sourcemaps.init())
        .pipe(less())
        //.pipe(cleanCSS())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(function (file) {
            log('----------------------------------');
            log('File ' + color(file.path.replace(/^.*[\\\/]/, ''), 'CYAN') + ' compiled');
            return file.base;
        }))
        .on('end', () => {
            log('==================================');
            log(color('Theme CSS compiled', 'GREEN') + '\n')
        })
});

gulp.task('compile-layers', () => {
    log('Strarting layers compilation' + '\n')
    return gulp.src('./layers/**/*.less')
        .pipe(sourcemaps.init())
        .pipe(less())
        //.pipe(cleanCSS())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(function (file) {
            log('----------------------------------');
            log('File ' + color(file.path.replace(/^.*[\\\/]/, ''), 'CYAN') + ' compiled');
            return file.base;
        }))
        .on('end', () => {
            log('==================================');
            log(color('Layers CSS compiled', 'GREEN') + '\n')
        })
});

// compiles all lees into one css file
// gulp.task('compile-dist', () => {
//     return gulp.src('./*.less')
//         .pipe(sourcemaps.init())
//         .pipe(less())
//         .pipe(sourcemaps.write())
//         .pipe(gulp.dest("./css/theme-compiled.css"))
//         .on('end', () => {log('Theme CSS compiled, bundled and minified')})
// });
