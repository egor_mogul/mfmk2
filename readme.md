# MFMK2 README   


//////////////////////////////////////////////////////////////////////////////

## Preparing local dev environment.

1. Install [node,js](https://nodejs.org/en/). Download and follow instructions.

2. Copy theme files to your local PC.

3. Check env tools in the command line by typing *node -v* / *npm -v* if returns error, get to nodejs.org and install the latest stable node.js(npm is included).

4. pen(shift + right click) either console or power shell in the theme folder.
    - Type *npm install* in the console and wait a bit. You should see installation progress. If the command line is launched in the wrong directory the command returns error.

5. If you're woking on the remote server, launch synchronization(FileZilla/winSCP option - need to exclude node_modules(i used "don't transfer hidden folder" WinSCP option for .git and node_modules folders) folder and git service files).

6. Ready, you're awesome!!1


### Compiling LESS to CSS.

Commands list:
*gulp watch* - watches all the styling changes made in LESS files and compiles them on changes
(to end the batch task press ctrl + c few times). Basically, do the installation steps above and tell gulp to watch changes.

Manual commands:
- *gulp compile* - compiles all LESS files to CSS.
- *gulp compile-theme* - compiles only main theme file.
- *gulp compile-layers* - compiles only layers files.

Gulp returns compilation errors with filename and line, so keep an eye on the console.

TODO: pre deploy CSS/JS bundling/minifying?

If needed to compile and minify all CSS into one file look into gulpfile.js in the root, I left commented function for this. haven't tested yet but should be working )


### Copying/easy renaming layers
Copy folder, open it, select all the files in it, press F2 and do rename(this will rename all selected files).

## DOCUMENTATION LINKS
- [Current layers list](https://docs.google.com/spreadsheets/d/1azqVvf7KURfasM-pz-dJz6m3-JGCIY4X_ZijfisYKnE/edit#gid=359888750)
- [Animations libraries and examples](https://docs.google.com/document/d/1AAkiuvsabuHalVUTx5xi8FFFfz44qzfEY_7RVOHJe-w/edit#heading=h.hgi2qomej9g)
- [ACFs notes(likely outdated)](https://docs.google.com/document/d/1sVZvlYCzaIULnJp3-5RpkEY9Ero1j3pvLEYzvDQx2iU/edit)
- [CSS framework](https://docs.google.com/document/d/1lYdZOMeSI5Yv5CPOdWQFbdnHYuNMAXpa8Gj2BzNO2Pg/edit#heading=h.8lfdi6vmey02)
- [Figma UI constructor\(to be updated\)](https://www.figma.com/file/JtNuVxPEGgqDvp7lwTxAm6/UI-constructor?node-id=2%3A158)


## LESS
### LESS-structure

There are a few core principles:

1. each layer has its own encapsulated LESS/CSS file. Or injected mixin if feasible.
2. changes in the theme styles are basically pulled from Figma UI constructor.


All styles are broken down to components:

- vars.less - all global variables. _No global variables to be declared everywhere else. See the comments in the file_.
- grid.less - grid system:
- etc, the structure quite intuitive

# HTML structure
1. .grid__ element(main.less).
2. .block_ (or vue component)(main.less).
3. .element(semantic name, all appearance rules attached)(layername.less).
4. .\_modifier(global style with functional/semantic name + related appearance rules(cancel button is red because it interrupts something, it doesn’t cancel something only because it’s red)). Block or element(and its children) specific modifier.
5. .\-modifier (layer/block/element-specific appearance modifier + children + nth padding\margins modifiers, sizes, colors). To be stored in layer's LESS file or in mixins.less. The point is that all store all custom stuff in modifiers/mixins.
6. .\__state(block/element-specific semantic name:\__disabled, \__active, etc)

7. v-\* vue transition classes.
8. j-\* JS-linked classes(sliders, filtering, animation scripts etc). Classes to be use for scripts initialization, settings to be linked to data-attributes.


## HTML grid structure

## TODO:
We're missing:
* alternating(the latest version in the current framework is quite good). (repeater/cloned existing alternating ASF structure).
* custom post selector

///////////////////////////////////////////

LESS/CSS stuff upgrades
1. add mixins.
2. add px to em/rem automatic conversion.
3. prepare basic layers(from existing Figma UI page).

//////////////////////////////////////////

# PHP layers folders structure:

1. PHP-file
2. LESS file -> CSS
3. JS file (think about bundling into one file/store only parameters and call them as callback after creating instance from the main file).
4. JSON ACF export.

\+ cloned layers if needed(as separate JSON files).

NB: this way we need to have all the layers in the child theme, ideally with a layer for each unique layer instance.

# ACFs
## ACF JSON and layers library

The idea behind ACF changing:
- keep all the field names consistent across all layers, so title is always section/repeating item title.
- make layers/fields groups reusable.
- separate views from data

All ACF structure to be stored in JSON files, ready for importing.

*IMPORTANT*
There are repeating ACFs in JSON, for instance: background-colors

JSON example:

"-bg-color-brand1": "Main Brand colour",
"-bg-color-brand2": "Secondary Brand colour",
"-bg-blue": "Blue(example)",
"-bg-orange": "Orange(example)"

JSON key is the value of select = CSS class applied to layer.
JSON value is label in ACF's UI select.

Easy solution would be using "replace in folder" functionality provided by IDE.

#### Search for the entire JSON section:

"-bg-color-brand1": "Main Brand colour",
"-bg-color-brand2": "Secondary Brand colour",
"-bg-blue": "Blue(example)",
"-bg-orange": "Orange(example)"

#### And replace this with any key:value pair you want:

"-bg-black": "Black",
"-bg-gray": "Gray"

Then make adjustment to theme.less and vars.less, then import ACF JSON.
Alternative is going through all the ACFs and replacing the code manually.

## Layers type description:
1. Static(default intros, CTAs etc) - no loops in the template.
2. Repeater(cards, tiles).
    - Alternating - repeating flexible content.
    - Custom post repeaters.   

# Design process
Figma file with UI kit(in Mogul Figma files)
- Duplicate the UI kit and design concepts.
- Update the kit if needed. NB: be sure that all updates are in UI kit(components, styles etc) and not just copied from concepts.

## Dev process
The main rule - each unique layer(design and/or functionality) to have its own PHP layer/ACF instance. This way any changes can be done without affecting the rest of layers.

ACFs:
- duplicate appropriate existing field layer template(static/repeating/alternating/custom post selector etc).
- modify sublayers if needed(those applied globally). There is an option to create parent fields group with the same name as sub-layer and clone only fields you need(instead or just cloning sub-layer).
- You can create ACF from scratch.

### PHP functions short reference:

FUNCTIONS:
1. field("field-name", "tag-name", "class") - shortcut for serviceField("fields")
Checks if field and value exists and outputs either tags with custom class and values from ACF or just values if the last two parameters are empty.
    + "field-name" ACF value - required
    + "tag-name" tag in template - optional
    + "class" CSS class -optional

returns <tag-name class="class">field-name</tag-name>

<code>
example: &#60;?php>field("title", "h1", "small");?>
</code>
<br>
<code>
returns: &#60;h1> class="small">ACF_title_value&#60;/h1>
</code><br><br>


## FE process
Modify global variables, add common stuff to mixins, inject mixins to layers LESS, add layer specific code.

#  Methodology
1. Layers names to be descriptive enough for the client and maintenance. I.e. "Products" instead of just "titles".
2. Each layer with unique design and/or functionality to be a single instance.
3. Each custom made layer to be stored in the library.  
    - rename folder/files.
    - duplicate ACF, check for any theme specific settings(i.e. replace all colors selects labels/values to default-default).
    - create wireframe pic and put this into the folder.
    - create text file with short description/instructions/plugins/scripts dependencies.
    - rename section class in both .php and .less files(i.e. "products" in *section* and "product" in *div.col* to layer name (i.e. regular_tiles_slider)).

Don't put back into library layers that were taken from there and slightly modified(columns amount changing/adding a static button/slight layout alteration). Or layers with heaps custom functionality/design.
