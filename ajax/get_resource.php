<?php

	//Takes ajax requests from the sales page, geocodes the supplied address, searches for installers nearby
	require_once('../../../../wp-load.php');

	$max_distance = 1000;
	$result_limit = 9;

	$response = new stdClass();
	if(!isset($_GET['searchlat']) || !isset($_GET['searchlng'])) { 
		$response->message = 'No lat/lng supplied';
		$response->status = 'error';
		echo json_encode($response);
		exit;
	}
	$searchlat = urldecode($_GET['searchlat']);
	$searchlng = urldecode($_GET['searchlng']);
	$resource_type = urldecode($_GET['resource_type']);


	//get all our resources
	$args = array(
		'post_type' => $resource_type,
		'posts_per_page' => '-1'

	);
	$resources = get_posts($args);

	//SELECT id, ( 3959 * acos( cos( radians(37) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(-122) ) + sin( radians(37) ) * sin( radians( lat ) ) ) ) AS distance FROM markers HAVING distance < 25 ORDER BY distance LIMIT 0 , 20;
	//Now find resources close enough using haversine formula
	$response_resources = array(); //all resources, as HTML to use in case we have no nearby search results
	$close_resources = array(); //the resources close ($max_distance km) to the search, with HTML

	foreach($resources as $resource) {

		if(get_field('location', $resource->ID)) {
			$lat = get_field('location', $resource->ID)['lat'];
			$lng = get_field('location', $resource->ID)['lng'];	

			$degreeRadius = deg2rad(1);

			//haversine formula
			$distance = ( 6371 * acos( cos( ($searchlat*$degreeRadius) ) * cos( ( $lat * $degreeRadius ) ) * cos( ( $lng * $degreeRadius ) - ($searchlng*$degreeRadius) ) + sin( ($searchlat*$degreeRadius) ) * sin( ( $lat * $degreeRadius ) ) ) );
			
			//echo $distance; //in km
			//if($distance < $max_distance) { //result
				$response_resource = new stdClass();
				$response_resource->distance = $distance;
				$response_resource->lat = $lat;
				$response_resource->lng = $lng;

				$response_resource->markup = '
				 	<div class="col tile shadow resource-tile">
    				<div class="tile-copy">
							<h4><a href="' . get_permalink( $resource->ID ) . '">' . $resource->post_title . '</a></h4>
							<p>
								<strong>Address:</strong> ' . get_field('location', $resource->ID)['address'] . '<br>';
	
								if( get_field('phone', $resource->ID) ){
									$response_resource->markup .= '<strong>Phone:</strong> <a href="tel:' . get_field('phone', $resource->ID) . '">' . get_field('phone', $resource->ID) . '</a>';
								}

								if( get_field('email', $resource->ID) ){
									$response_resource->markup .= '<strong>Email:</strong> <a href="mailto:' . get_field('email', $resource->ID) . '">' . get_field('email', $resource->ID) . '</a>';
								}

								if( get_field('website', $resource->ID) ){
									$response_resource->markup .= '<strong>Website:</strong> <a href="' . get_field('website', $resource->ID) . '">' . get_field('website', $resource->ID) . '</a>';
								}
		
							$response_resource->markup .= '</p>
						</div><!-- .tile-copy -->
  				</div><!-- .col -->';

				$response_resources[] = $response_resource;
			//}
		}		
	}//end foreach stores

	if(count($response_resources)) {
		usort($response_resources,'sort_resources');
		$response->status = 'ok';
		$response->resources = array_slice($response_resources,0,$result_limit);
		echo json_encode($response);
	} else {
		$response->message = 'None were found';
		$response->status = 'fail';
		echo json_encode($response);
	}


	function sort_resources($a,$b) {
		if ($a->distance == $b->distance) {
	        return 0;
	    }
	    return ($a->distance < $b->distance) ? -1 : 1;
	}
